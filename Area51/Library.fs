namespace Area51

open ProviderImplementation.ProvidedTypes
open FSharp.Core.CompilerServices
open System.Reflection

module TestModule =

    let testFunc() =
        
        //let x = Microsoft.CodeAnalysis.Compilation()
        //x.
        ()


[<TypeProvider>]
type BasicGenerativeProvider (config : TypeProviderConfig) as this =
    inherit TypeProviderForNamespaces (config, assemblyReplacementMap=[("BasicGenerativeProvider.DesignTime", "BasicProvider")])

    let ns = "BasicProvider"
    let asm = Assembly.GetExecutingAssembly()

    let createType typeName (count:int) =
        let asm = ProvidedAssembly()
        let myType = ProvidedTypeDefinition(asm, ns, typeName, Some typeof<obj>, isErased=false)

        let ctor = ProvidedConstructor([], invokeCode = fun args -> <@@ "My internal state" :> obj @@>)
        myType.AddMember(ctor)

        let ctor2 = ProvidedConstructor([ProvidedParameter("InnerState", typeof<string>)], invokeCode = fun args -> <@@ (%%(args.[1]):string) :> obj @@>)
        myType.AddMember(ctor2)

        for i in 1 .. count do 
            let prop = ProvidedProperty("Property" + string i, typeof<int>, getterCode = fun args -> <@@ count @@>)
            myType.AddMember(prop)

        //let meth = ProvidedMethod("StaticMethod", [], typeof<BasicProvider.Helpers.SomeRuntimeHelper>, isStatic=true, invokeCode = (fun args -> Expr.Value(null, typeof<BasicProvider.Helpers.SomeRuntimeHelper>)))
        //myType.AddMember(meth)
        //asm.AddTypes [ myType ]

        myType

    let myParamType = 
        let t = ProvidedTypeDefinition(asm, ns, "GenerativeProvider", Some typeof<obj>, isErased=false)
        t.DefineStaticParameters( [ProvidedStaticParameter("Count", typeof<int>)], fun typeName args -> createType typeName (unbox<int> args.[0]))
        t
    do
        this.AddNamespace(ns, [myParamType])
